#include "rbfm.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include <cstdio>
#include <cassert>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

RecordBasedFileManager* RecordBasedFileManager::_rbf_manager = 0;

RecordBasedFileManager* RecordBasedFileManager::instance()
{
    if(!_rbf_manager)
        _rbf_manager = new RecordBasedFileManager();

    return _rbf_manager;
}

RecordBasedFileManager::RecordBasedFileManager()
{
}

RecordBasedFileManager::~RecordBasedFileManager()
{
}

RC RecordBasedFileManager::createFile(const string &fileName) 
{
    const char *chFileName = fileName.c_str();
    return PagedFileManager::instance()->createFile(chFileName);
}

RC RecordBasedFileManager::destroyFile(const string &fileName) 
{
    const char *chFileName = fileName.c_str();
    return PagedFileManager::instance()->destroyFile(chFileName);
}

RC RecordBasedFileManager::openFile(const string &fileName, FileHandle &fileHandle) 
{
    const char *chFileName = fileName.c_str();
    return PagedFileManager::instance()->openFile(chFileName, fileHandle);
}

RC RecordBasedFileManager::closeFile(FileHandle &fileHandle) 
{
    return PagedFileManager::instance()->closeFile(fileHandle);
}

RC RecordBasedFileManager::insertRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, RID &rid) 
{
    if(recordDescriptor.size() == 0)
    {
        perror("Invalid recordDescriptor");
        assert(false);
        return -1;
    }

    int occupiedSlots = (int)(getRecordLenth(recordDescriptor, data) / SLOT_SIZE) + 1;
    char firstSlot = findInsertLocation(fileHandle, occupiedSlots, data, rid);

    char *pageData = new char[PAGE_SIZE];
    fileHandle.readPage(rid.pageNum, pageData);
    memcpy((void *)(pageData + firstSlot*SLOT_SIZE), (char *)data, getRecordLenth(recordDescriptor, data));


    insertRID(firstSlot, occupiedSlots, pageData, rid);
    fileHandle.writePage(rid.pageNum, pageData);
    free(pageData);

    return 0;
}

RC RecordBasedFileManager::readRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid, void *data) 
{
    char *pageData = new char[PAGE_SIZE];

    if(fileHandle.readPage(rid.pageNum, pageData)<0)
    {
        perror("readRecord");
        fprintf(stderr, "Page %d does not exist.\n", rid.pageNum);
        return -1;
    }

    char firstSlot;
    char occupiedSlots;
    resolveRID(&firstSlot, &occupiedSlots, pageData, rid);

    char *recordData = (char *)(pageData + firstSlot*SLOT_SIZE);
    memcpy((void *)data, recordData, getRecordLenth(recordDescriptor, recordData));

    free(pageData);

    return 0;
}

RC RecordBasedFileManager::printRecord(const vector<Attribute> &recordDescriptor, const void *data) 
{
    if(recordDescriptor.size() == 0)
    {
        return -1;
    }
    int offset = 0;

    char *varChar;
    for (unsigned int i=0; i<recordDescriptor.size(); i++)
    {
        switch (recordDescriptor[i].type)
        {
            case TypeInt:
                printf("Interger: %d\t", getIntData(offset, data));
                offset += 4;
                break;

            case TypeReal:
                printf("Real: %f\t",getRealData(offset, data));
                offset += 4;
                break;

            case TypeVarChar:
                int varCharLength;
                memcpy(&varCharLength, (char *)data + offset, sizeof(int));
                offset += 4;
                varChar = new char[100];
                getVarCharData(offset, data, varChar, varCharLength);
                printf("VarChar Length: %d\t", varCharLength);
                printf("VarChar: %s\t", varChar);
                offset += varCharLength;
                free(varChar);
                break;

            default:
                return -1;
        }
    }
    printf("\n");

    return 0;
}

int RecordBasedFileManager::getIntData(int offset, const void* data)
{
    int intData;
    memcpy(&intData, (char *)data + offset, sizeof(int));
    return intData;
}
float RecordBasedFileManager::getRealData(int offset, const void* data)
{
    float realData;
    memcpy(&realData, (char *)data + offset, sizeof(float));
    return realData;
}
RC RecordBasedFileManager::getVarCharData(int offset, const void* data, char* varChar, int varCharLength)
{
    memcpy((void *)varChar, (char *)data + offset, varCharLength);
    varChar[varCharLength] = '\0';
    return 0;
}

int RecordBasedFileManager::getRecordLenth(const vector<Attribute> &recordDescriptor, const void *data)
{
    int offset = 0;
    for(unsigned int i=0; i<recordDescriptor.size(); i++)
    {
        if (recordDescriptor[i].type == TypeVarChar)
        {
            int varCharLength;
            memcpy(&varCharLength, (char *)data + offset, sizeof(int));
            offset += varCharLength;
        }
        offset += 4;
    }

    return offset;
}

char RecordBasedFileManager::findInsertLocation(FileHandle &fileHandle, char occupiedSlots, const void* data, RID &rid)
{
    unsigned metaPageNum = -1;
    unsigned totalPages = fileHandle.getNumberOfPages();
    unsigned formalTotalPages = totalPages;
    char firstSlot;
    char *metaPage = new char[PAGE_SIZE];

    for(unsigned i=0; ; i++)
    {
        //  Load next meta page.
        if((i % PAGES_PER_SEGMENT) == 0)
        {
            metaPageNum++;
            if((i == totalPages) && (totalPages != 0))
            {
                fileHandle.insertMetaPage(metaPageNum);
                fileHandle.getMetaPage(metaPageNum, metaPage);
            }
            else
            {
                fileHandle.getMetaPage(metaPageNum, metaPage);
            }
        }

        if(i == totalPages)
        {
            fileHandle.initializePage(i);
            totalPages++;
        }

        firstSlot = getFirstAvailableSlot(i, metaPage);


        if(firstSlot + occupiedSlots < NUM_OF_SLOT)
        {
            rid.pageNum = i;
            setFirstAvailableSlot(rid.pageNum, metaPage, firstSlot + occupiedSlots);
            // printf("pageNum: %d, firstSlot: %d\n", i, firstSlot);
            fileHandle.updateMetaPage(metaPageNum, metaPage);

            // printf("totalPages: %d, formalTotalPages: %d\n", totalPages, formalTotalPages);
            if(totalPages != formalTotalPages)
            {
                fileHandle.setNumberOfPages(totalPages);
            }
            
            break;
        }
    }

    return firstSlot;
}

char RecordBasedFileManager::getFirstAvailableSlot(PageNum pageNum, const void *data)
{
    char result;
    unsigned offset = pageNum%PAGES_PER_SEGMENT;
    memcpy(&result, (char *)data+offset, sizeof(char));
    return result;
}

RC RecordBasedFileManager::setFirstAvailableSlot(PageNum pageNum, void *data, char slotNum)
{
    unsigned offset = pageNum%PAGES_PER_SEGMENT;
    memcpy((char *)data+offset, &slotNum, sizeof(char));

    return 0;
}

RC RecordBasedFileManager::insertRID(char firstSlot, char occupiedSlots, void *data, RID &rid)
{
    rid.slotNum = getFirstIdleRID(data);

    int offset = PAGE_SIZE - 2*(PAGE_SIZE / SLOT_SIZE) + 2*rid.slotNum;

    memcpy((char *)data+offset, &firstSlot, sizeof(char));
    memcpy((char *)data+offset+1, &occupiedSlots, sizeof(char));

    return 0;
}

RC RecordBasedFileManager::resolveRID(char *firstSlot, char *occupiedSlots, const void *data, const RID &rid)
{
    int offset = PAGE_SIZE - 2*(PAGE_SIZE / SLOT_SIZE) + 2*rid.slotNum;

    memcpy(firstSlot, (char *)data+offset, sizeof(char));
    memcpy(occupiedSlots, (char *)data+offset+1, sizeof(char));

    return 0;
}

char RecordBasedFileManager::getFirstIdleRID(const void *data)
{
    int offset = PAGE_SIZE - 2*(PAGE_SIZE / SLOT_SIZE);
    char occupiedRID;
    char location;
    for(location=0; location<(PAGE_SIZE / SLOT_SIZE - 8); location++)
    {
        occupiedRID = *((char *)data + offset + 2*location + 1);
        if(occupiedRID == 0)
        {
            break;
        }
    }

    return location;
}