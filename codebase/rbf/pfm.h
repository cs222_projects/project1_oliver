#ifndef _pfm_h_
#define _pfm_h_

typedef int RC;
typedef unsigned PageNum;

#define PAGE_SIZE 4096
#define PAGES_PER_SEGMENT 4000

class FileHandle;


class PagedFileManager
{
public:
    static PagedFileManager* instance();                     // Access to the _pf_manager instance

    RC createFile    (const char *fileName);                         // Create a new file
    RC destroyFile   (const char *fileName);                         // Destroy a file
    RC openFile      (const char *fileName, FileHandle &fileHandle); // Open a file
    RC closeFile     (FileHandle &fileHandle);                       // Close a file

protected:
    PagedFileManager();                                   // Constructor
    ~PagedFileManager();                                  // Destructor

private: 
    static PagedFileManager *_pf_manager;
    int fd;
};


class FileHandle
{
public:
    FileHandle();                                                    // Default constructor
    ~FileHandle();                                                   // Destructor

    RC readPage(PageNum pageNum, void *data);                           // Get a specific page
    RC writePage(PageNum pageNum, const void *data);                    // Write a specific page
    RC appendPage(const void *data);                                    // Append a specific page
    RC initializePage(PageNum pageNum);

    unsigned getNumberOfPages();                                        // Get the number of pages in the file
    RC setNumberOfPages(PageNum pageNum);

    RC getMetaPage(PageNum metaPageNum, void *data);
    RC insertMetaPage(PageNum metaPageNum);
    RC updateMetaPage(PageNum metaPageNum, const void *data);

    int fd;
    RC initializeFile();

private:
    RC increaseNumberOfPages();
    
    RC writePageToFile(const void *data);
    RC readPageFromFile(void *data);

    RC seekPage(PageNum pageNum);
    RC seekMetaPage(PageNum pageNum);
    unsigned long gotoEnd();
    RC gotoNumOfPages();
    RC createAnEmptyPage(void *data);
 };

 #endif
