#include "pfm.h"
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cassert>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h> 

using namespace std;

PagedFileManager* PagedFileManager::_pf_manager = 0;

PagedFileManager* PagedFileManager::instance()
{
    if(!_pf_manager)
        _pf_manager = new PagedFileManager();

    return _pf_manager;
}


PagedFileManager::PagedFileManager()
{
	fd = 0;
}


PagedFileManager::~PagedFileManager()
{
}


RC PagedFileManager::createFile(const char *fileName)
{
	fd = open(fileName, O_RDWR | O_CREAT | O_EXCL, 0777);

	if(fd != -1)
	{
		FileHandle fileHandle;
		fileHandle.fd = fd;
		fileHandle.initializeFile();
		closeFile(fileHandle);

		return 0;
	}
	else
	{
		return -1;
	}
}


RC PagedFileManager::destroyFile(const char *fileName)
{
	return remove(fileName);
}


RC PagedFileManager::openFile(const char *fileName, FileHandle &fileHandle)
{
	fd = open(fileName, O_RDWR);

	if(fd != -1)
	{
		fileHandle.fd = fd;
	}
	else
	{
		return -1;
	}

	return 0;
}


RC PagedFileManager::closeFile(FileHandle &fileHandle)
{
    return close(fileHandle.fd);
}


FileHandle::FileHandle()
{
	fd = 0;
}


FileHandle::~FileHandle()
{
}


RC FileHandle::readPage(PageNum pageNum, void *data)
{
	if(seekPage(pageNum)<0)
	{
		return -1;
	}

	if(readPageFromFile(data)<0)
	{
		perror("readPageFromFile");
		fprintf(stderr, "Bytes Read: %lu Page Size: %d\n", sizeof(data), PAGE_SIZE);
		return -1;
	}

	return 0;
}


RC FileHandle::writePage(PageNum pageNum, const void *data)
{
    if(seekPage(pageNum)<0)
	{
		return -1;
	}

	return writePageToFile(data);
}


RC FileHandle::appendPage(const void *data)
{
    if(gotoEnd()<0)
	{
		perror("appendPage");
		assert(false);
		return -1;
	}

	if(writePageToFile(data) == 0)
	{
		increaseNumberOfPages();
		return 0;
	}
	else
	{
		return -1;
	}
}

RC FileHandle::readPageFromFile(void *data)
{
	unsigned int bytesRead = read(fd, data, PAGE_SIZE);
	if(bytesRead != PAGE_SIZE)
	{
		perror("read");

		fprintf(stderr, "Bytes Read: %u Page Size: %u\n", bytesRead, PAGE_SIZE);

		return -1;
	}

	return 0;
}

RC FileHandle::writePageToFile(const void *data)
{
	unsigned int bytesWritten = write(fd, data, PAGE_SIZE);

	if(bytesWritten != PAGE_SIZE)
	{
		perror("write");

		fprintf(stderr, "Bytes Written: %u Page Size: %u\n", bytesWritten, PAGE_SIZE);

		return -1;
	}

	return 0;
}

RC FileHandle::initializePage(PageNum pageNum)
{
	char *pageData = new char[PAGE_SIZE];
	createAnEmptyPage(pageData);

	if(pageNum < getNumberOfPages())
	{
		writePage(pageNum, pageData);
	}
	else
	{
		appendPage(pageData);
	}
	free(pageData);

	return 0;
}

RC FileHandle::seekPage(PageNum pageNum)
{
	if(pageNum >=getNumberOfPages())
	{
		return -1;
	}

	unsigned long offset = (pageNum + (int)(pageNum / PAGES_PER_SEGMENT) + 1) * PAGE_SIZE;

    if((lseek(fd, offset, SEEK_SET)<0))
	{
		perror("lseek");
		assert(false);
		return -1;
	}

	return 0;
}

RC FileHandle::seekMetaPage(PageNum metaPageNum)
{
	unsigned long offset = metaPageNum * PAGE_SIZE * (PAGES_PER_SEGMENT + 1);

    if((lseek(fd, offset, SEEK_SET)<0))
	{
		perror("lseek");
		assert(false);
		return -1;
	}

	return 0;
}

unsigned long FileHandle::gotoEnd()
{
	unsigned long totalBytes = lseek(fd, 0, SEEK_END);
    if(totalBytes < 0)
    {
    	perror("gotoEnd");
		assert(false);
		return -1;
    }
    else
    {
    	return totalBytes;
    }
}


unsigned FileHandle::getNumberOfPages()
{
	if(gotoNumOfPages()<0)
	{
		perror("gotoNumOfPages");
		assert(false);
		return -1;
	}

	unsigned result;

	char *data = new char[sizeof(unsigned)];
	unsigned int bytesRead = read(fd, data, sizeof(unsigned));
	if(bytesRead != sizeof(unsigned))
	{
		perror("getNumberOfPages");
		fprintf(stderr, "Bytes Read: %u Size of int: %lu\n", bytesRead, sizeof(unsigned));
		return -1;
	}

	memcpy(&result, (char *)data, sizeof(unsigned));

	free(data);

	return result;
}

RC FileHandle::setNumberOfPages(PageNum pageNum)
{
	if(gotoNumOfPages()<0)
	{
		perror("gotoNumOfPages");
		assert(false);
		return -1;
	}

	char *data = new char[sizeof(int)];
	memcpy(data, (char *)&pageNum, sizeof(int));
	unsigned int bytesWritten = write(fd, data, sizeof(int));

	if(bytesWritten != sizeof(int))
	{
		perror("setNumberOfPages");
		fprintf(stderr, "Bytes Written: %u Size of int: %lu\n", bytesWritten, sizeof(int));
		free(data);

		return -1;
	}

	free(data);

	return 0;
}

RC FileHandle::increaseNumberOfPages()
{
	unsigned totalPages = getNumberOfPages();

	totalPages++;
	
	setNumberOfPages(totalPages);

	if((totalPages+1) % PAGES_PER_SEGMENT == 0)
	{
		insertMetaPage((int)((totalPages+1) / PAGES_PER_SEGMENT));
	}

	return 0;
}

RC FileHandle::insertMetaPage(PageNum metaPageNum)
{
	if(gotoEnd()<0)
	{
		perror("insertMetaPage");
		assert(false);
		return -1;
	}

	char *newPage = new char[PAGE_SIZE];
	createAnEmptyPage(newPage);
	writePageToFile(newPage);
	free(newPage);

	return 0;
}

RC FileHandle::getMetaPage(PageNum metaPageNum, void *data)
{
	if(seekMetaPage(metaPageNum)<0)
	{
		perror("seekMetaPage");
		assert(false);
		return -1;
	}

	if(readPageFromFile(data)<0)
	{
		perror("readPageFromFile");
		fprintf(stderr, "Bytes Read: %lu Page Size: %u\n", sizeof(data), PAGE_SIZE);
		return -1;
	}

	return 0;
}

RC FileHandle::updateMetaPage(PageNum metaPageNum, const void *data)
{
	if(seekMetaPage(metaPageNum)<0)
	{
		perror("seekMetaPage");
		assert(false);
		return -1;
	}

	writePageToFile(data);

	return 0;
}

RC FileHandle::gotoNumOfPages()
{
	int offset = PAGE_SIZE - sizeof(int);

	if(lseek(fd, offset, SEEK_SET)<0)
	{
		perror("lseek");
		assert(false);
		return -1;
	}

	return 0;
}


RC FileHandle::initializeFile()
{
	char *newPage = new char[PAGE_SIZE];
	
	createAnEmptyPage(newPage);

	int offset = PAGE_SIZE - sizeof(int);
	int pageNum = 0;
	memcpy((char *)(newPage + offset), &pageNum, sizeof(int));

    if((lseek(fd, 0, SEEK_SET)<0))
	{
		perror("lseek");
		assert(false);
		return -1;
	}

    writePageToFile(newPage);
    free(newPage);

    return 0;
}

RC FileHandle::createAnEmptyPage(void *data)
{
	memset(data, 0, PAGE_SIZE);
	return 0;
}